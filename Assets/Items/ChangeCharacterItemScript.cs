﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;

public class ChangeCharacterItemScript : MonoBehaviour {

	// Use this for initialization
	Rigidbody2D _rigid2D = null;
	void Start () {
		_rigid2D = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.tag.Equals ("Player")) {
			GameGlobal.getInstance ().GameManager.ChangeCharacter ("c_girl");
			GameGlobal.getInstance ().MainCamera.ChangeTarget (GameGlobal.getInstance ().GameManager.character);
			Destroy (gameObject);
		}
	}
}
