﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ColorBestScore : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GetComponent<Text>().text = ScoreModel.bestScore.ToString();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
