﻿using UnityEngine;
using System.Collections;

public class TestCameraScript : MonoBehaviour {

	public Vector2 _direction;

	// Use this for initialization
	void Start () {
	
	}
	
	void Update () {
		
		transform.position = new Vector3 (transform.position.x + _direction.x * Time.deltaTime, transform.position.y + _direction.y * Time.deltaTime,
			transform.position.z);

		// delegate to BackgroundBaseController
		//		base.Update();

	}
}
