﻿using UnityEngine;
using System.Collections;

public class ZichZac2 : MonoBehaviour {
	public float speed;
	public Vector2 direct;
	public float rotateSpeed;
	public GameObject shadow;
	public float time;
	public float step;

	public AudioClip[] listAudio;
	private AudioSource audioSource;

	Rigidbody2D _rigid2D;
	Vector2 _currentDirect;
	float currentTime = 0;

	private bool isSwitchDirection = false;
	private float target_step = 0;

	// Use this for initialization
	void Start () {
		_rigid2D = GetComponent<Rigidbody2D> ();
		_currentDirect = direct;
		target_step = transform.position.y + step;
		this.audioSource = this.GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (0)) {
			isSwitchDirection = true;
		}

		if (isSwitchDirection && transform.position.y >= target_step && direct.y > 0) {
			swithDirection ();
			isSwitchDirection = false;
			target_step = transform.position.y - step;
		} else if (isSwitchDirection && transform.position.y <= target_step && direct.y < 0) {
			swithDirection ();
			isSwitchDirection = false;
			target_step = transform.position.y + step;
		}

		currentTime += Time.deltaTime;
		if (currentTime > 0.05f) {
			Instantiate (shadow, transform.position, transform.rotation);
			currentTime = 0;
		}
	}

	void FixedUpdate() {
		_currentDirect = Vector2.Lerp (_currentDirect, direct, 1);
		Debug.Log (_currentDirect.ToString());
		Vector2 vel = calcVel (_currentDirect, speed);
		_rigid2D.velocity = new Vector2 (speed, vel.y);
	}

	Vector2 calcVel(Vector2 direction, float speed) {
		float length = direction.magnitude;
		float ratio = speed / length;
		return new Vector2 (direction.x * ratio, direction.y * ratio);
	}

	void swithDirection() {
		direct.y = -direct.y;
		if (direct.y > 0) {
			audioSource.PlayOneShot (listAudio [0]);
		} else {
			audioSource.PlayOneShot (listAudio [1]);
		}
	}
}
