﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class CharacterManager {
	public const string CHARACTER_CURRENT_KEY = "CHARACTER_CURRENT_KEY";

	private static CharacterManager _instance;
	public static CharacterManager getInstance() {
		if (_instance == null)
			_instance = new CharacterManager ();
		return _instance;
	}



	// Use this for initialization
	private string _currentCharacter;

	public CharacterManager() {
		_init ();
	}

	public bool IsUnlockCharacter(GameObject character) {
		string id = character.GetComponent<CharaterDesignScript> ().identify;
		return IsUnlockCharacter (id);

	}

	public bool IsUnlockCharacter(string identify) {
		return PlayerPrefs.HasKey ("unlock_" + identify);
	}

	public void UnlockCharacter(GameObject character) {
		string id = character.GetComponent<CharaterDesignScript> ().identify;
		UnlockCharacter (id);
	}

	public void UnlockCharacter(string identify) {
		PlayerPrefs.SetInt ("unlock_" + identify, 1);
	}

	public string CurrentCharacter {
		get {
			return _currentCharacter;
		}
		set {
			_currentCharacter = value;
			PlayerPrefs.SetString (CHARACTER_CURRENT_KEY, _currentCharacter);
		}
	}

	void _init() {
		// current character
		CurrentCharacter = PlayerPrefs.GetString (CHARACTER_CURRENT_KEY, string.Empty);
		PlayerPrefs.SetString (CHARACTER_CURRENT_KEY, CurrentCharacter);
	}
}
