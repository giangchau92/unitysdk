﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;

public class CameraFollowPlayer : BaseCameraController {
	public float offsetX = 0;
	// Use this for initialization
	Vector3 _lastCharaterPosition;
	Vector3 _direction;
	GameObject _target;
	void Awake() {
		GameGlobal.getInstance ().MainCamera = this;
	}

	void Start () {
	}

	// Update is called once per frame
	void Update () {
		_lazyInit();

		Vector3 distance = _target.transform.position - _lastCharaterPosition;
		_lastCharaterPosition = _target.transform.position;
		Vector2 distanceInDirect = new Vector3 (distance.x, distance.x / _direction.x * _direction.y);
		transform.position = new Vector3 (transform.position.x + distanceInDirect.x, transform.position.y + distanceInDirect.y,
			transform.position.z);

		// delegate to BackgroundBaseController
//		base.Update();

	}

	void _lazyInit() {
		if (_target == null) {
			GameObject firstLevel = GameGlobal.getInstance ().GameManager.getLastShowingLevel ();
			_direction = LevelUtils.getEndPositionInWorld (firstLevel) - LevelUtils.getStartPositionInWorld (firstLevel);

			_target = GameGlobal.getInstance ().GameManager.character;
			_lastCharaterPosition = _target.transform.position;
		}
	}

	public void ChangeTarget(GameObject target) {
		this._target = target;
	}
}