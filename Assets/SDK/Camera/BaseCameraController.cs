﻿using System;
using UnityEngine;

namespace AssemblyCSharp
{
	public class BaseCameraController : MonoBehaviour
	{
		public Transform startPoint;
		public Transform endPoint;

		void OnDrawGizmosSelected() {

			Gizmos.color = Color.white;
			Vector3 size = (endPoint.position - startPoint.position);
			size = new Vector3 (Math.Abs (size.x), Math.Abs (size.y), Math.Abs (size.z));
			Vector3 center = (startPoint.position + endPoint.position) / 2;
			Gizmos.DrawWireCube(center, size);
		}

		public Bounds getCameraRangeBounds() {
			Vector3 size = (endPoint.position - startPoint.position);
			size = new Vector3 (Math.Abs (size.x), Math.Abs (size.y), Math.Abs (size.z));
			Vector3 center = (startPoint.position + endPoint.position) / 2;
			Bounds result = new Bounds (center, size);
			return result;
		}
	}
}

