﻿using UnityEngine;
using System.Collections;

public class HDRelativeScript : MonoBehaviour {
	public enum AnchorType
	{
		TopLeft,
		TopRight,
		BotRight,
		BotLeft
	}

	public GameObject target;
	public AnchorType anchor;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnDrawGizmosSelected() {
		SpriteRenderer rend = target.GetComponent<SpriteRenderer> ();
		Vector3 center = rend.bounds.center;
		Gizmos.color = Color.white;
		Gizmos.DrawWireCube(center, new Vector3(rend.bounds.extents.x * 2, rend.bounds.extents.y * 2, 0));
	}
		
}
