﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(HDRelativeScript))]
public class HDRelativeScriptEditor : Editor {
	SerializedProperty anchorPro;
	SerializedProperty targetPro;

	MonoScript script;

	void OnEnable() {
		script = MonoScript.FromMonoBehaviour((HDRelativeScript)target);

		anchorPro = serializedObject.FindProperty ("anchor");
		targetPro = serializedObject.FindProperty ("target");
	}

	public override void OnInspectorGUI() {
		serializedObject.Update ();

		EditorGUI.BeginDisabledGroup (true);
		script = EditorGUILayout.ObjectField("Script:", script, typeof(MonoScript), false) as MonoScript;
		EditorGUI.EndDisabledGroup ();

		EditorGUILayout.Space ();
		EditorGUILayout.PropertyField (anchorPro);
		EditorGUILayout.PropertyField (targetPro);
		if (GUILayout.Button ("Do It")) {

			SpriteRenderer _render = ((GameObject)targetPro.objectReferenceValue).GetComponent<SpriteRenderer> ();
			if (_render == null) {
				Debug.LogError ("Target must have SpriteRender component");
			} else {
				Bounds bounds = _render.bounds;
//				HDRelativeScript.AnchorType type
				Vector3 pos = Vector3.zero;
				int type = anchorPro.intValue;
				if (type == (int)HDRelativeScript.AnchorType.TopLeft) {
					pos = new Vector3 (bounds.center.x - bounds.extents.x, bounds.center.y + bounds.extents.y, bounds.center.z);
				} else if (type == (int)HDRelativeScript.AnchorType.TopRight) {
					pos = new Vector3 (bounds.center.x + bounds.extents.x, bounds.center.y + bounds.extents.y, bounds.center.z);
				} else if (type == (int)HDRelativeScript.AnchorType.BotLeft) {
					pos = new Vector3 (bounds.center.x - bounds.extents.x, bounds.center.y - bounds.extents.y, bounds.center.z);
				} else {
					pos = new Vector3 (bounds.center.x + bounds.extents.x, bounds.center.y - bounds.extents.y, bounds.center.z);
				}
//				bounds.center - bounds.extents;
				(target as HDRelativeScript).gameObject.transform.position = pos;
			}
		}

		serializedObject.ApplyModifiedProperties ();
	}
}
#endif