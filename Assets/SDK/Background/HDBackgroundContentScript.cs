﻿using UnityEngine;
using System.Collections;

public class HDBackgroundContentScript : MonoBehaviour {
	public Transform startPoint;
	public Transform endPoint;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public float getMaxX() {
		return endPoint.position.x;
	}
	public float getMinX() {
		return startPoint.position.x;
	}

	public float getLenghtX() {
		return Mathf.Abs (endPoint.position.x - startPoint.position.x);
	}
}
