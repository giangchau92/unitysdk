﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using AssemblyCSharp;
using System.Linq;

public class HDBackgroundScript : MonoBehaviour {
	public GameObject content;
	public float scaleFactor = 1;

	private List<GameObject> _listContents = new List<GameObject>();
	Vector3 _lastCameraPostion;
	// Use this for initialization
	void Start () {
		_listContents.Add (content);
		_lastCameraPostion = GameGlobal.getInstance ().MainCamera.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		// Generation and Destruction
		Bounds cameraBounds = GameGlobal.getInstance ().MainCamera.getCameraRangeBounds ();
		float cameraMax = cameraBounds.max.x;
		float cameraMin = cameraBounds.min.x;

		HDBackgroundContentScript _contentMaxScript = _listContents.Last ().GetComponent<HDBackgroundContentScript> ();
		float contentMax = _contentMaxScript.getMaxX ();
		HDBackgroundContentScript _contentMinScript = _listContents.First ().GetComponent<HDBackgroundContentScript> ();
		float contentMin = _contentMinScript.getMaxX ();

		/* scaleFactor sẽ xác định chiều di chuyển của background so với camera
		 * TH1: di chuyển ngược
		 * 		- Tạo ra bg mới và add vào last
		 * 		- Huỷ bg đã vượt ra khỏi tầm camera
		 * TH2: di chuyển xuôi
		 * 		- Tạo ra bg mới và add vào first
		 * 		- Huỷ bg đã vượt ra khỏi tầm camera
		 */
		if (cameraMax > contentMax && scaleFactor > 0) {
			// clone content
			GameObject newContent = Instantiate (_listContents.Last ());
			float contentLength = _contentMaxScript.getLenghtX ();
			newContent.transform.position = _listContents.Last ().transform.position + new Vector3 (contentLength, 0, 0);
			newContent.transform.parent = _listContents.Last ().transform.parent;
			_listContents.Add (newContent);
		} else if (cameraMin < contentMin && scaleFactor < 0) {
			GameObject newContent = Instantiate (_listContents.First ());
			float contentLength = _contentMaxScript.getLenghtX ();
			newContent.transform.position = _listContents.First ().transform.position - new Vector3 (contentLength, 0, 0);
			newContent.transform.parent = _listContents.First ().transform.parent;
			_listContents.Insert (0, newContent);
		}

		if (cameraMin > contentMin && scaleFactor > 0) {
			// remove content
			GameObject content = _listContents.First ();
			Destroy (content);
			_listContents.RemoveAt (0);
		} else if (cameraMax < _contentMaxScript.getMinX() && scaleFactor < 0) {
			GameObject content = _listContents.Last ();
			Destroy (content);
			_listContents.RemoveAt(_listContents.Count - 1);
		}

		// parallax
		Vector3 pos = GameGlobal.getInstance().MainCamera.transform.position;
		Vector3 distance = pos - _lastCameraPostion;
		_lastCameraPostion = pos;
		transform.position = transform.position - new Vector3 (distance.x * scaleFactor, transform.position.y, transform.position.z);
//		transform.position = new Vector3 ((transform.position.x - distance.x) * scaleFactor, transform.position.y, transform.position.z);
//		lastPosition = GameGlobal.getInstance ().MainCamera.transform.position;
//		transform.position = new Vector3 ((transform.position.x + (lastPosition.x - firstPosition.x)/scale), (transform.position.y + (lastPosition.y - firstPosition.y)/scale), transform.position.z);
//		firstPosition = lastPosition;
	}

//	private Bounds getContentBound() {
//		Vector3 size = (endPoint.position - startPoint.position);
//		size = new Vector3 (Math.Abs (size.x), Math.Abs (size.y), Math.Abs (size.z));
//		Vector3 center = (startPoint.position + endPoint.position) / 2;
//		Bounds result = new Bounds (center, size);
//		return result;
//	}
}
