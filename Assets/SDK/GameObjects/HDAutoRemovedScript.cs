﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;

/*
 * Đối tượng sẽ tự Destroy khi vị trí nó ra ngoài main camera
 * Nên dùng cho những đối tượng nhỏ. Vì nó lấy tranform position để tính toán
 * và ko quan tâm đến kích thước của đối tượng
 */
public class HDAutoRemovedScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Bounds cameraBounds = GameGlobal.getInstance ().MainCamera.getCameraRangeBounds ();
		float cameraMin = cameraBounds.min.x;

		if (cameraMin > transform.position.x) {
			// remove content
			Destroy(gameObject);
		}
	}
}
