﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using AssemblyCSharp;

public class HDGameManagerScript : MonoBehaviour {
	public Vector2 gravity = new Vector2(0, -9.8f);
	public List<GameObject> listLevel = new List<GameObject>();
	public GameObject character;
	public Transform characterPosition;
	public GameObject mainCamera;
	public float discoverOffset = 10;
	// for level dessign maping
	public GameObject level1;
	public GameObject level2;

	[SerializeField]
	List<GameObject> _showingLevels;
	HDCharacterManagerScript _characterScript;
	bool _isPaused;

	void Awake () {
		QualitySettings.vSyncCount = 0;  // VSync must be disabled
		Application.targetFrameRate = 60;
		Physics2D.gravity = gravity;

		GameGlobal.getInstance ().GameManager = this;
		GameGlobal.getInstance ().IsRunning = true;
	}

	void Start () {
		GameObject firstLevel = GameObject.FindGameObjectWithTag ("Start");
		Debug.Assert (firstLevel != null, "No entry level");

		// xóa tất cả prototype design trên màn hình, tất cả chúng đều đc lưu vào List
		_clearAllLevelPrototype();

		_showingLevels = new List<GameObject> ();
		_showingLevels.Add (firstLevel);
		_genarateNextLevel (listLevel.First());
		// genarate character
		_characterScript = GetComponent<HDCharacterManagerScript>();
		string currentCha = CharacterManager.getInstance().CurrentCharacter;
		if (currentCha.Equals (string.Empty)) {
			GameObject chaObj = _characterScript.characters.First ();
			string id = chaObj.GetComponent<CharaterDesignScript> ().identify;
			CharacterManager.getInstance ().CurrentCharacter = id;
			// instantiate character
			this.character = (GameObject)Instantiate (chaObj, characterPosition.position, chaObj.transform.rotation);
			//
		} else {
			bool has = false;
			for (int i = 0; i < _characterScript.characters.Count; i++) {
				GameObject obj = _characterScript.characters.ElementAt (i);
				if (obj.GetComponent<CharaterDesignScript> ().identify.Equals (currentCha)) {
					this.character = (GameObject)Instantiate (obj, characterPosition.position, obj.transform.rotation);
					has = true;
				}
			}
			if (!has) {
				GameObject obj1 = _characterScript.characters.First ();
				string id = obj1.GetComponent<CharaterDesignScript> ().identify;
				this.character = (GameObject)Instantiate (obj1, characterPosition.position, obj1.transform.rotation);
				CharacterManager.getInstance ().CurrentCharacter = id;
			}
		}
//		GameGlobal.getInstance().MainCamera.transform.parent = this.character.transform;
	}
	
	// Update is called once per frame
	void Update () {
		// kiểm tra camera và sinh level
		GameObject lastMap = _getLastShowingLevel();
		Vector3 endPoint = LevelUtils.getEndPositionInWorld (lastMap);
		Vector3 playerPos = this.character.transform.position;
		if (playerPos.x + discoverOffset >  endPoint.x) { // tam thời kiểm tra theo trục x, còn trường hợp chung thì chưa biết
			_genarateNextLevel (_randomLevel());
		}

		// kiểm tra xóa bớt level đã vượt qua
		_checkAndRemoveLevelPassed();

	}

	public void Pause(bool paused) {
		_isPaused = paused;
		Time.timeScale = paused ? 0 : 1;
	}

	public GameObject getLastShowingLevel() {
		return _getLastShowingLevel ();
	}

	public void ChangeCharacter(string characterId, bool keepPhysic = true) {
		
		for (int i = 0; i < _characterScript.characters.Count; i++) {
			GameObject cha = _characterScript.characters.ElementAt (i);
			if (cha.GetComponent<CharaterDesignScript>().identify.Equals(characterId)) {
				Vector3 pos = character.transform.position;
				Quaternion rot = character.transform.rotation;
				Vector2 vel = Vector2.zero;
				Rigidbody2D rigid2 = character.GetComponent<Rigidbody2D> ();
				if (rigid2 != null) {
					vel = rigid2.velocity;
				}
				//
				Destroy(this.character);
				//
				this.character = Instantiate(cha);
				this.character.transform.position = pos;
				this.character.transform.rotation = rot;
				Rigidbody2D rigid2Des = this.character.GetComponent<Rigidbody2D> ();
				if (rigid2Des && keepPhysic) {
					rigid2Des.velocity = vel;
				}
				return;
			}
		}
		Debug.Log("Invalid character id " + characterId);
	}
		
	/* Tạo ra level từ bản mẫu
	 * Và set vị trí cjo nó nối tiếp nhau
	 */
	void _genarateNextLevel(GameObject prototype) {
		Debug.Assert (prototype != null, "Level prototype null");
		try {
			GameObject lastLevel = _getLastShowingLevel ();
			Debug.Assert (lastLevel != null, "No entry level");

			Vector3 endPoint = LevelUtils.getEndPositionInWorld(lastLevel);
			Vector3 startPoint = LevelUtils.getStartPosition(prototype);

			Vector3 rightPosition = endPoint - startPoint;

//			Debug.Log("EndPoint: " + endPoint.ToString() + " StartPoint: " + startPoint.ToString() + " Pos: " + rightPosition.ToString());

			GameObject map = (GameObject)Instantiate(prototype, rightPosition, prototype.transform.rotation);
			map.SetActive(true);
			_clearGameLevel(map);
			_showingLevels.Add(map);

		} catch (System.Exception ex) {
			Debug.Log ("Can not genarate new map" + ex.Message);
			return;
		}

	}
		

	/* Lấy màn chơi cuối cùng đang hiện trên màn hình
	 * Sữ dụng để lấy màn chơi tiếp theo
	 */
	GameObject _getLastShowingLevel() {
		if (_showingLevels.Count == 0)
			return null;
		return _showingLevels.Last();
	}

	/* Kiểm tra trong level mẫu nếu có character hoặc */
	void _clearGameLevel(GameObject gameLevel) {
		for (int i = 0; i < gameLevel.transform.childCount; i++) {
			GameObject child = gameLevel.transform.GetChild (i).gameObject;
			if (LevelUtils.isCharacter (child) || LevelUtils.isCamera (child))
				Destroy (child);
			
		}
	}


	/* Xóa tất cả  object mà là Level Mẫu */
	void _clearAllLevelPrototype() {
		GameObject[] levels = GameObject.FindGameObjectsWithTag ("Level");
		for (int i = 0; i < levels.Count(); i++) {
			//Destroy (levels.ElementAt(i));
			levels.ElementAt (i).SetActive (false);
		}
	}

	/*
	 * Random level
	 */
	GameObject _randomLevel() {
		Debug.Assert (listLevel.Count > 0, "No prototype level");
		int total = 0;
		foreach (var item in listLevel) {
			HDLevelController lvlScript = item.GetComponent<HDLevelController> ();
			if (lvlScript == null)
				continue;
			total += lvlScript.ratio;
		}
		int index = Random.Range (0, total + 1);
		foreach (var item in listLevel) {
			HDLevelController lvlScript = item.GetComponent<HDLevelController> ();
			if (lvlScript == null)
				continue;
			index -= lvlScript.ratio;
			if (index <= 0)
				return item;
		}
		return listLevel.First();
	}


	void _checkAndRemoveLevelPassed() {
		while (true) {
			if (_showingLevels.Count == 0)
				break;
			GameObject map = _showingLevels.First ();
			Vector3 endPos = LevelUtils.getEndPositionInWorld (map);
			Vector3 playerPos = this.character.transform.position;
			if (playerPos.x - discoverOffset > endPos.x) {
				_showingLevels.RemoveAt (0);
				Destroy (map);
			} else
				break;
		}
	}

	/* GET SET*/
	public bool IsPaused {
		get {
			return _isPaused;
		}
	}
}
