﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;

public class ThePitScript : CharacterBaseScript {
	// Use this for initialization
	public Vector2 ForceJump = new Vector2(500, 0);
	public Vector2 Speed = new Vector2 (0, 0);
	public int jumpCount = 1;
	public int stateRun = 0;
	public int stateJump = 0;
	public int stateDown = 0;
	public int stateDie = 0;

	Rigidbody2D _body;
	Animator _animator;
	int _expectedState = 0; // 0 normal, -1 down, 1 jump
	int _state = 0;
	int _jumCount  = 0;
	//Transform _transform;

	void Start () {
		_body = GetComponent<Rigidbody2D> ();
		_animator = GetComponent<Animator> ();
		//_transform = GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () {
//		if (Input.GetKeyDown (KeyCode.A))
//			_jump ();
//		
//		if (Input.touchCount > 0) {
//			Touch touch = Input.GetTouch (0);
//			if (touch.phase == TouchPhase.Began)
//				_jump ();
//		}
		if (Input.GetMouseButtonDown (0)) {
			_expectedState = -1;
			if (_state == 0) {
				_down ();
			}

		} else if (Input.GetMouseButtonUp (0)) {
			_expectedState = 0;
			if (_state == -1) {
				_jump ();
			} else if (_state == 1) {
				_jump ();
			}
		}
	}

	void FixedUpdate() {
		_body.velocity = new Vector2(Speed.x, _body.velocity.y);
	}

	void _jump() {
		if (_jumCount < jumpCount) {
			_body.velocity = new Vector2(_body.velocity.x, ForceJump.y);
			_animator.SetInteger ("state", stateJump);
			_jumCount++;
			_state = 1;
		}
		Debug.Log ("Jump");
	}

	void _run() {
		_animator.SetInteger ("state", stateRun);
		_state = 0;
//		_expectedState = 0;
	}

	void _down() {
		_animator.SetInteger ("state", stateDown);
		_state = -1;
		Debug.Log ("Down");
	}

	void _onLand() {
		_jumCount = 0;
		if (_expectedState == -1)
			_down ();
		else if (_expectedState == 0)
			_run ();
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag.Equals ("Land")) {
			if (_expectedState != _state || _state == 1) {
				_onLand ();
			}
		} else if (coll.gameObject.tag.Equals ("Enemy")) {
		}
	}
}
