﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;

public class HDEnemyScript : MonoBehaviour {

	public int animationStateDie;
	// Use this for initialization
	Animator _animator;
	void Start () {
		_animator = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnCollisionEnter2D(Collision2D other) {
		if (other.gameObject.tag.Equals ("Player")) {
			GameGlobal.getInstance ().Replay ();
			_die ();
		}
	}

	#region PRIVATE FUNC

	void _die() {
		if (_animator)
			_animator.SetInteger ("state", animationStateDie);
	}

	#endregion
}
