﻿using UnityEngine;
using System.Collections;

public class RotateByAction : ActionBaseScript {
	public float rotation;
	public float duration;
	private float timeLine = 0;

	// Use this for initialization
	public override void init ()
	{
		timeLine = 0;
	}

	// Update is called once per frame
	void Update () {
		if (timeLine < duration) {
			_target.transform.Rotate (Vector3.back, rotation / duration * Time.deltaTime);
			timeLine += Time.deltaTime;
		} else {
			isFinished = true;
		}
	}
}
