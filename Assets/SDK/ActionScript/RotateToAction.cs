﻿using UnityEngine;
using System.Collections;

public class RotateToAction : ActionBaseScript {
	public float rotation;
	public float duration;
	private float timeLine = 0;

	private float rotationReal;
	// Use this for initialization
	public override void init ()
	{
		timeLine = 0;
		rotationReal = rotation + _target.transform.rotation.z;
	}

	// Update is called once per frame
	void Update () {
		if (timeLine < duration) {
			_target.transform.Rotate (Vector3.back, rotationReal / duration * Time.deltaTime);
			timeLine += Time.deltaTime;
		} else {
			isFinished = true;
		}
	}
}
