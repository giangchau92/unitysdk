﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RepeatAction : ActionBaseScript {
	public int count;
	public List<ActionBaseScript> actions;

	// Use this for initialization
	private List<ActionBaseScript> _actionsClone;
	private int _countClone;

	public override void init() {
		if (count == 0) {
			isFinished = true;
			return;
		}

		_actionsClone = new List<ActionBaseScript>(actions);
		_countClone = count;
		startRemainAction ();
		_countClone--;
	}
	
	// Update is called once per frame
	void Update () {
		if (count == 0) {
			return;
		}
		if (_actionsClone.Count == 0)
			startRemainAction ();
		else {
			while (_actionsClone.Count > 0) {
				ActionBaseScript script = _actionsClone [0];
				if (script.isFinished) {
					_actionsClone.RemoveAt (0);
					startRemainAction ();
				} else {
					break;
				}
			}
		}
	}

	void startRemainAction() {
		if (_actionsClone.Count > 0) {
			ActionBaseScript script = _actionsClone [0];
			script.setTarget (this._target);
			script.init ();
			script.enabled = true;
			int a;
			a = 1;
		} else {
			if (_countClone > 0 || _countClone < 0) {
				_countClone--;
				_actionsClone = new List<ActionBaseScript> (actions);
				resetAction (_actionsClone);
				startRemainAction ();
			} else {
				isFinished = true;
			}
		}
	}

	List<ActionBaseScript> cloneAction(List<ActionBaseScript> list) {
		List<ActionBaseScript> result = new List<ActionBaseScript> ();
		foreach (ActionBaseScript item in list) {
			result.Add (Instantiate (item));
		}
		return result;
	}

	void resetAction(List<ActionBaseScript> list) {
		foreach (ActionBaseScript item in list) {
			item.isFinished = false;
			item.enabled = false;
		}
	}
}
