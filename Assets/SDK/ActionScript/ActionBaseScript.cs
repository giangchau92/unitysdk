﻿using UnityEngine;
using System.Collections;

public class ActionBaseScript : MonoBehaviour {
	public bool owner = true;
	public bool isFinished = false;
	protected GameObject _target;
	// Use this for initialization
	void Start () {
		if (!owner) {
			this.enabled = false;
			return;
		} else {
			_target = gameObject;
			this.init ();
		}
	}
	public virtual void init () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void setTarget(GameObject obj) {
		_target = obj;	
	}
}
