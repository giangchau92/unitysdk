﻿using UnityEngine;
using System.Collections;

public class MoveToAction : ActionBaseScript {
	public Vector3 position;
	public float duration;
	private Vector3 _v;
	private Vector3 _s;
	private float timeLine = 0;
	// Use this for initialization

	public override void init ()
	{
		_s = position - _target.transform.position;
		_v = _s / duration;
		timeLine = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (timeLine < duration) {
			_target.transform.position = new Vector3 (_target.transform.position.x + _v.x * Time.deltaTime, _target.transform.position.y + _v.y * Time.deltaTime, _target.transform.position.z);
			timeLine += Time.deltaTime;
		} else {
			isFinished = true;
		}
	}
}
