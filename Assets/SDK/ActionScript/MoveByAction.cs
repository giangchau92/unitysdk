﻿using UnityEngine;
using System.Collections;

public class MoveByAction : ActionBaseScript {
	public Vector3 distance;
	public float duration;
	private Vector3 _v;
	private float timeLine = 0;
	// Use this for initialization
	public override void init ()
	{
		_v = distance / duration;
		timeLine = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (timeLine < duration) {
			_target.transform.position = new Vector3 (_target.transform.position.x + _v.x * Time.deltaTime, _target.transform.position.y + _v.y * Time.deltaTime, _target.transform.position.z);
			timeLine += Time.deltaTime;
		} else {
			isFinished = true;
		}
	}
}
