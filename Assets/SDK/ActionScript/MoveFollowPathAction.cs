﻿using UnityEngine;
using System.Collections;

public class MoveFollowPathAction : ActionBaseScript {
	public BezierSpline bezier;
	public float duration;

	private float currentTime = 0;

	public override void init ()
	{
		currentTime = 0;
		_target.transform.position = bezier.GetPoint (0);
	}
	
	// Update is called once per frame
	void Update () {
		if (currentTime < duration) {
			_target.transform.position = bezier.GetPoint (currentTime / duration);
			currentTime += Time.deltaTime;
		} else {
			isFinished = true;
		}
	}
}
