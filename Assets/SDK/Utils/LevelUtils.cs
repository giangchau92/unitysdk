﻿using System;
using UnityEngine;

namespace AssemblyCSharp
{
	public class LevelUtils
	{
		public LevelUtils ()
		{
		}

		public static Vector3 getStartPositionInWorld(GameObject levelObject) {
			HDLevelController lvlController = levelObject.GetComponent<HDLevelController> ();
			Debug.Assert (lvlController != null, "Level dont have LevelController component");

			return lvlController.startPoint.position;
		}

		public static Vector3 getStartPosition(GameObject levelObject) {
			HDLevelController lvlController = levelObject.GetComponent<HDLevelController> ();
			Debug.Assert (lvlController != null, "Level dont have LevelController component");

			return lvlController.startPoint.localPosition;
		}

		public static Vector3 getEndPositionInWorld(GameObject levelObject) {
			HDLevelController lvlController = levelObject.GetComponent<HDLevelController> ();
			Debug.Assert (lvlController != null, "Level dont have LevelController component");

			return lvlController.endPoint.position;
		}

		public static Vector3 getEndPosition(GameObject levelObject) {
			HDLevelController lvlController = levelObject.GetComponent<HDLevelController> ();
			Debug.Assert (lvlController != null, "Level dont have LevelController component");

			return lvlController.endPoint.localPosition;
		}

		public static bool isCharacter(GameObject gameObject) {
			return gameObject.tag == "Player";
		}

		public static bool isCamera(GameObject gameObject) {
			return gameObject.tag == "MainCamera";
		}
	}
}

