﻿using System;
using UnityEngine;

namespace AssemblyCSharp
{
	public class GameObjectUtils
	{
		public GameObjectUtils ()
		{
		}
		public bool isComponentExist<T>(GameObject gameObject) {
			return gameObject.GetComponent<T> () != null;
		}
	}
}

