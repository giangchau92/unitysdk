﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace AssemblyCSharp
{
	public class GameGlobal
	{
		private GameGlobal ()
		{
		}


		static GameGlobal _sharedInstance;
		public static GameGlobal getInstance() {
			if (_sharedInstance == null) {
				_sharedInstance = new GameGlobal ();
			}
			return _sharedInstance;
		}


		HDGameManagerScript _gameSetting;
		public HDGameManagerScript GameManager {
			get {
				return _gameSetting;
			}
			set {
				Debug.Assert (_gameSetting == null, "GameSetting exist");
				_gameSetting = value;
			}
		}

//		MainGameUIScript _gameUI;
//		public MainGameUIScript GameUI {
//			get {
//				return _gameUI;
//			}
//			set {
//				Debug.Assert (_gameUI == null, "GameUI exist");
//				_gameUI = value;
//			}
//		}

		HDScoreManagerScript _scoreManager;
		public HDScoreManagerScript ScoreManager {
			get {
				return _scoreManager;
			}

			set {
				Debug.Assert (_scoreManager == null, "ScoreManager exist");
				_scoreManager = value;
			}
		}

		CameraFollowPlayer _baseCamera;
		public CameraFollowPlayer MainCamera {
			get {
				return _baseCamera;
			}
			set {
				Debug.Assert (_baseCamera == null, "MainCamera exist");
				_baseCamera = value;
			}
		}

		private bool _isRunning = false;
		public bool IsRunning {
			get {
				return _isRunning;
			}
			set {
				_isRunning = value;
			}
		}


		#region PUBLIC FUNCTION

		public void Replay() {
			Scene scene = SceneManager.GetActiveScene(); 
			SceneManager.LoadScene (scene.name);
		}

		#endregion
	}
}

