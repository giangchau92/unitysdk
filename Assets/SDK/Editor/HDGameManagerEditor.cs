﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditor.SceneManagement;
using Rotorz.ReorderableList;
using AssemblyCSharp;

[CustomEditor(typeof(HDGameManagerScript))]
public class HDGameManagerEditor : Editor {

	string[] requireTags = {"Start", "Level", "Land", "Enemy"};

	SerializedProperty gravityPro;
	SerializedProperty listLevelPro;
	SerializedProperty characterPositionPro;
	SerializedProperty mainCameraPro;

	SerializedProperty discoverOffsetPro;

	SerializedProperty level1Pro;
	SerializedProperty level2Pro;

	MonoScript script;

	public void OnEnable() {
		script = MonoScript.FromMonoBehaviour((HDGameManagerScript)target);
		gravityPro = serializedObject.FindProperty ("gravity");
		listLevelPro = serializedObject.FindProperty ("listLevel");
		characterPositionPro = serializedObject.FindProperty ("characterPosition");
		mainCameraPro = serializedObject.FindProperty ("mainCamera");

		discoverOffsetPro = serializedObject.FindProperty ("discoverOffset");

		level1Pro = serializedObject.FindProperty ("level1");
		level2Pro = serializedObject.FindProperty ("level2");

		foreach (var item in requireTags) {
			addTag (item);
		}
	}

	public override void OnInspectorGUI() {
		serializedObject.Update ();

		EditorGUI.BeginDisabledGroup (true);
		script = EditorGUILayout.ObjectField("Script:", script, typeof(MonoScript), false) as MonoScript;
		EditorGUI.EndDisabledGroup ();

		// Anount UI Script
		EditorGUILayout.HelpBox ("For UI event handle Add new Script", MessageType.Warning);
		EditorGUILayout.Space ();

		if (!_isSDKInited ()) {
			EditorGUILayout.HelpBox ("SDK Component missing", MessageType.Error);
			if(GUILayout.Button("Init SDK"))
			{
				_initializeComponent ();
			}
		} else {
			if (getScript ().listLevel.Count == 0) {
				EditorGUILayout.HelpBox ("Level missing", MessageType.Error);
			}
			EditorGUILayout.PropertyField (gravityPro);
			ReorderableListGUI.Title("List Level");
			ReorderableListGUI.ListField (listLevelPro);
			if (characterPositionPro.objectReferenceValue == null) {
				EditorGUILayout.HelpBox ("Charactor position missing", MessageType.Error);
			}
			EditorGUILayout.PropertyField (characterPositionPro);
			EditorGUILayout.PropertyField (mainCameraPro);
			EditorGUILayout.PropertyField (discoverOffsetPro);
		}
		// for editor
		if (GUILayout.Button ("Character Editor")) {
			EditorSceneManager.OpenScene ("Assets/SDK/Scenes/CharacterEditor.unity");
		}

		if (GUILayout.Button ("Create Level")) {
			Instantiate (Resources.Load("SDK/Level1Prefab"));
		}

		EditorGUILayout.Space ();
		EditorGUILayout.HelpBox ("Draw 2 level and press Link", MessageType.Info);
		EditorGUILayout.PropertyField (level1Pro);
		EditorGUILayout.PropertyField (level2Pro);
		if (GUILayout.Button ("Link")) {
			GameObject lvl1 = level1Pro.objectReferenceValue as GameObject;
			GameObject lvl2 = level2Pro.objectReferenceValue as GameObject;

			Vector3 endPoint = LevelUtils.getEndPositionInWorld(lvl1);
			Vector3 startPoint = LevelUtils.getStartPosition(lvl2);

			Vector3 rightPosition = endPoint - startPoint;
			lvl2.transform.position = rightPosition;
		}

		serializedObject.ApplyModifiedProperties ();
	}

	private bool _isSDKInited() {
		bool checkScore = _isHasComponent<HDScoreManagerScript> ();
		bool checkCharacter = _isHasComponent<HDCharacterManagerScript> ();
		bool checkObj = _isRequireObjectInited ();
		bool isRunning = GameGlobal.getInstance ().IsRunning;
		return checkScore && checkCharacter && checkObj;
	}

	#region SDK INIT CHECK

	bool _isHasComponent<T>() {
		return (target as HDGameManagerScript).gameObject.GetComponent<T> () != null;
	}

	bool _isRequireObjectInited() {
		bool checkStart = GameObject.FindGameObjectWithTag ("Start");
		return checkStart;
	}

	void _initializeComponent() {
		if (!_isHasComponent<HDScoreManagerScript> ()) {
			(target as HDGameManagerScript).gameObject.AddComponent<HDScoreManagerScript> ();
			Debug.Log ("HDScoreManagerScript created");
		}
		if (!_isHasComponent<HDCharacterManagerScript> ()) {
			(target as HDGameManagerScript).gameObject.AddComponent<HDCharacterManagerScript> ();
		}
		// init required object
		if (GameObject.FindGameObjectWithTag ("Start") == null) {
			Instantiate (Resources.Load("SDK/StartPrefab"));
		}
	}

	#endregion

	#region PRIVATE FUNC
	HDGameManagerScript getScript() {
		return (target as HDGameManagerScript);
	}
	#endregion

	bool _isTagExist(string tag) {
		SerializedObject tagManager = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/TagManager.asset")[0]);
		SerializedProperty tagsProp = tagManager.FindProperty("tags");

		// Adding a Tag
		string s = tag;

		// First check if it is not already present
		bool found = false;
		for (int i = 0; i < tagsProp.arraySize; i++)
		{
			SerializedProperty t = tagsProp.GetArrayElementAtIndex(i);
			if (t.stringValue.Equals(s)) { found = true; break; }
		}
		return found;
	}

	void addTag(string tag) {
		SerializedObject tagManager = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/TagManager.asset")[0]);
		SerializedProperty tagsProp = tagManager.FindProperty("tags");

		// Adding a Tag
		string s = tag;

		// First check if it is not already present
		bool found = false;
		for (int i = 0; i < tagsProp.arraySize; i++)
		{
			SerializedProperty t = tagsProp.GetArrayElementAtIndex(i);
			if (t.stringValue.Equals(s)) { found = true; break; }
		}

		// if not found, add it
		if (!found)
		{
			tagsProp.InsertArrayElementAtIndex(0);
			SerializedProperty n = tagsProp.GetArrayElementAtIndex(0);
			n.stringValue = s;
			tagManager.ApplyModifiedProperties();
			tagManager.Update ();
		}
	}

}
