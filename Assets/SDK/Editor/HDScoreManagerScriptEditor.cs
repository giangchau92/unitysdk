﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(HDScoreManagerScript))]
[CanEditMultipleObjects]
public class HDScoreManagerScriptEditor : Editor {
	public SerializedProperty startCoinPro;
	public SerializedProperty scoreTypePro;
	public SerializedProperty pointAwardDistancePro;
	public SerializedProperty pointAwardPerDistancePro;
	public SerializedProperty pointAwardTimePro;
	public SerializedProperty pointAwardPerTimePro;


	// for distance score type 
	MonoScript script;

	void OnEnable() {
		scoreTypePro = serializedObject.FindProperty ("scoreType");
		startCoinPro = serializedObject.FindProperty ("startCoin");
		pointAwardDistancePro = serializedObject.FindProperty ("pointAwardDistance");
		pointAwardPerDistancePro = serializedObject.FindProperty ("pointAwardPerDistance");
		pointAwardTimePro = serializedObject.FindProperty ("pointAwardTime");
		pointAwardPerTimePro = serializedObject.FindProperty ("pointAwardPerTime");

		script = MonoScript.FromMonoBehaviour((HDScoreManagerScript)target);
	}

	public override void OnInspectorGUI() {
		serializedObject.Update ();

		EditorGUI.BeginDisabledGroup (true);
		script = EditorGUILayout.ObjectField("Script:", script, typeof(MonoScript), false) as MonoScript;
		EditorGUI.EndDisabledGroup ();
		EditorGUILayout.PropertyField (startCoinPro);

		EditorGUILayout.Space ();
		EditorGUILayout.PropertyField (scoreTypePro);
		HDScoreManagerScript.ScoreType type = (HDScoreManagerScript.ScoreType)scoreTypePro.enumValueIndex;
		if (type == HDScoreManagerScript.ScoreType.Distance) {
			EditorGUILayout.PropertyField (pointAwardDistancePro);
			EditorGUILayout.PropertyField (pointAwardPerDistancePro);
		} else if (type == HDScoreManagerScript.ScoreType.Time) {
			EditorGUILayout.PropertyField (pointAwardTimePro);
			EditorGUILayout.PropertyField (pointAwardPerTimePro);
		}

		

		serializedObject.ApplyModifiedProperties ();
	}


}
