﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif
using AssemblyCSharp;

#if UNITY_EDITOR
[CustomEditor(typeof(HDCharacterManagerScript))]
public class HDCharacterManagerScriptEditor : Editor {

	public override void OnInspectorGUI() {
		DrawDefaultInspector ();
	}
}
#endif