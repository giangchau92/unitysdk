﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(CharaterDesignScript))]
public class CharactorDesignScriptEditor : Editor {
	public SerializedProperty identifyPro;
	public SerializedProperty coinUnlockPro;

	MonoScript script;

	void OnEnable() {
		script = MonoScript.FromMonoBehaviour((CharaterDesignScript)target);

		identifyPro = serializedObject.FindProperty ("identify");
		coinUnlockPro = serializedObject.FindProperty ("coinUnlock");
	}

	public override void OnInspectorGUI() {
		serializedObject.Update ();

		EditorGUI.BeginDisabledGroup (true);
		script = EditorGUILayout.ObjectField("Script:", script, typeof(MonoScript), false) as MonoScript;
		EditorGUI.EndDisabledGroup ();

		if ((target as CharaterDesignScript).gameObject.GetComponent<Collider2D> () == null) {
			EditorGUILayout.HelpBox ("Charactor should have Collider2D based component", MessageType.Warning);
		}

		if ((target as CharaterDesignScript).gameObject.GetComponent<Rigidbody2D> () == null) {
			EditorGUILayout.HelpBox ("Charactor should have Rigidbody2D component", MessageType.Warning);
		}

		EditorGUILayout.PropertyField (identifyPro);
		EditorGUILayout.PropertyField (coinUnlockPro);

		serializedObject.ApplyModifiedProperties ();
	}
}
