﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using AssemblyCSharp;

[CustomEditor(typeof(EnemyDesignScript))]
public class EnemyDesignScriptEditor : Editor {

	MonoScript script;

	public void OnEnable() {
		script = MonoScript.FromMonoBehaviour ((EnemyDesignScript)target);
	}

	public override void OnInspectorGUI() {
		serializedObject.Update ();

		EditorGUI.BeginDisabledGroup (true);
		script = EditorGUILayout.ObjectField("Script:", script, typeof(MonoScript), false) as MonoScript;
		EditorGUI.EndDisabledGroup ();

		if ((target as EnemyDesignScript).gameObject.GetComponent<Collider2D> () == null) {
			EditorGUILayout.HelpBox ("Charactor should have Collider2D based component", MessageType.Warning);
		}

		serializedObject.ApplyModifiedProperties ();
	}
}
