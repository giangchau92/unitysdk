﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(HDCharatorEditorScript))]
public class HDCharatorEditorScriptEditor : Editor {

	MonoScript script;

	void OnEnable() {
		script = MonoScript.FromMonoBehaviour((HDCharatorEditorScript)target);
	}

	public override void OnInspectorGUI() {
		serializedObject.Update ();

		EditorGUI.BeginDisabledGroup (true);
		script = EditorGUILayout.ObjectField("Script:", script, typeof(MonoScript), false) as MonoScript;
		EditorGUI.EndDisabledGroup ();

		if (GUILayout.Button ("Create Character")) {
			Instantiate (Resources.Load("SDK/CharactorPrefab"));
		}

		if (GUILayout.Button ("Create Enemy")) {
			Instantiate (Resources.Load("SDK/EnemyPrefab"));
		}

		if (GUILayout.Button ("Create Item")) {
			Instantiate (Resources.Load("SDK/ItemPrefab"));
		}

		serializedObject.ApplyModifiedProperties ();
	}
}
