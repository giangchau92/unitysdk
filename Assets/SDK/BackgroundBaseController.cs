﻿using UnityEngine;
using System.Collections;

/*
 * Không thể add background trực tiếp vô camera đc vì những phần tử nào mà đc add vô camera thì
 * luôn đc vẽ sau mọi đối tượng. Ví dụ trường hợp mình cần background ở trước platform như game
 * The Pit thì ko ko thể làm đc
*/
public class BackgroundBaseController : MonoBehaviour {
//	public Transform camera;
//
//	Vector3 _lastCameraPostion;
	void Start () {
//		_lastCameraPostion = camera.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		// cho cái background base di chuyển theo background
//		Vector3 distance = this.camera.transform.position - _lastCameraPostion;
//		_lastCameraPostion = this.camera.transform.position;
//		transform.position = new Vector3 (transform.position.x + distance.x, transform.position.y + distance.y, transform.position.z);
	}

	public void onCameraMove(Vector3 distance) {
		BackgroundController[] listBg = gameObject.GetComponentsInChildren<BackgroundController> ();
		foreach (BackgroundController item in listBg) {
			item.onCameraMove (distance);
		}
	}
}
