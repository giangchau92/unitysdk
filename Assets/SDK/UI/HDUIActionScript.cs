﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class HDUIActionScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void GoToScene(string name) {
		SceneManager.LoadScene (name, LoadSceneMode.Single);
	}

	public void ShowPopup(string name) {
		SceneManager.LoadScene (name, LoadSceneMode.Additive);
	}
}
