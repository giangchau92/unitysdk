﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BackgroundController : MonoBehaviour {

	public GameObject content;
	public Transform startPoint;
	public Transform endPoint;
	public float scale = 1;
	public float offset = 0;

	List<GameObject> _listBackground;
	Rect bound;

	// Use this for initialization
	void Start () {
		_listBackground = new List<GameObject> ();
		_listBackground.Add (content);
//
//		SpriteRenderer render = content.GetComponent<SpriteRenderer> ();
//		bound = new Rect (render.bounds.min.x, render.bounds.min.y, render.sprite.bounds.size.x, render.sprite.bounds.size.y);
//
//		GameObject content2 = (GameObject)Instantiate (content);
//
//		content2.transform.parent = content.transform.parent;
//		content2.transform.localPosition = content.transform.localPosition + new Vector3 (bound.size.x * 2, 0, 0);
//		content2.transform.localScale = content.transform.localScale;
//		content2.transform.localRotation = content.transform.localRotation;
//		_listBackground.Add (content2);
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void onCameraMove(Vector3 distance) {
		Vector3 eulerRotation = transform.rotation.eulerAngles;
		Quaternion newRotation = Quaternion.Euler(-eulerRotation.x, -eulerRotation.y, -eulerRotation.z);
		Vector3 test = newRotation.eulerAngles;
		Vector3 result = newRotation * distance;
		Vector3 temp = result * scale;
		offset += temp.x * 10;
		if (offset > 1)
			offset -= 1;
		foreach (GameObject item in _listBackground) {
			//item.transform.localPosition -= temp;
			SpriteRenderer render = item.GetComponent<SpriteRenderer>();
			render.material.SetTextureOffset("_MainTex", new Vector2(offset, 0));
		}
//		content.transform.localPosition -= ;//new Vector3(0.1f, 0.0f, 0.0f);

	}
}
