﻿using UnityEngine;
using System.Collections;

public class HDLevelController : MonoBehaviour {

	public Transform startPoint;
	public Transform endPoint;
	public int ratio = 100;
	
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnDrawGizmos() {
		//		if (target != null) {
		if (startPoint != null && endPoint != null) {
			Gizmos.color = Color.white;
			Gizmos.DrawLine (startPoint.position, endPoint.position);
		}
	}
}
