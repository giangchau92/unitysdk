﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using AssemblyCSharp;

public class HDScoreManagerScript : MonoBehaviour {
	public int startCoin = 0;

	public ScoreType scoreType;
	public double pointAwardDistance = 1;
	public uint pointAwardPerDistance = 1;
	public double pointAwardTime = 1;
	public uint pointAwardPerTime = 1;

	// Use this for initialization
	int _userCoin;
	int _userScore;
	int _bestScore;
	int _lastScore;

	//
	List<ScoreShoingScriptDelegate> _scoreDelegates = new List<ScoreShoingScriptDelegate>();
	List<ScoreShoingScriptDelegate> _coinDelegates = new List<ScoreShoingScriptDelegate>();
	//
	ScoreBaseProcessor _processor;

	//
	public const string USER_COIN_KEY = "USER_COIN_KEY";
	public const string USER_BESTSCORE_KEY = "USER_BESTSCORE_KEY";
	public const string USER_LASTSCORE_KEY = "USER_LASTSCORE_KEY";

	public enum ScoreType
	{
		Distance,
		Time,
		Coin,
		None
	}

	// action for its delegate
	public enum ScoreAction
	{
		Score,
		Coin
	}

	void Awake() {
		GameGlobal.getInstance ().ScoreManager = this;
	}

	void Start () {
		_checkValidData ();
		_initData ();
		_initProcessor ();
	}
	
	// Update is called once per frame
	void Update () {
		if (_processor != null)
			_processor.Update (Time.deltaTime);
	}

	public void setBestScore(int score) {
		if (score <= _bestScore)
			return;
		_bestScore = score;
		ScoreModel.bestScore = _bestScore;
		PlayerPrefs.SetInt (USER_BESTSCORE_KEY, _bestScore);
	}

	public void setLastScore(int score) {
		_lastScore = score;
		PlayerPrefs.SetInt (USER_LASTSCORE_KEY, _lastScore);
	}

	public void changeCoin(int coin) {
		_userCoin += coin;
		PlayerPrefs.SetInt (USER_COIN_KEY, _userCoin);
		_dispatchCoin ();
	}

	public void changeScore(int score) {
		if (_processor == null) {
			Debug.Assert (_processor != null, "Can not change score when Processor null");
		}
		_userScore += score;
		_dispatchScore ();

		ScoreModel.score = _userScore;
		setBestScore (_userScore);
	}

	#region PRIVATE FUNC
	void _initData() {
		_userCoin = PlayerPrefs.GetInt (USER_COIN_KEY, -1);
		_bestScore = PlayerPrefs.GetInt (USER_BESTSCORE_KEY, -1);
		_lastScore = PlayerPrefs.GetInt (USER_LASTSCORE_KEY, -1);

		_userCoin = _userCoin == -1 ? startCoin : _userCoin;
		_bestScore = _bestScore == -1 ? 0 : _bestScore;
		_lastScore = _lastScore == -1 ? 0 : _lastScore;

		PlayerPrefs.SetInt (USER_COIN_KEY, _userCoin);
		PlayerPrefs.SetInt (USER_BESTSCORE_KEY, _bestScore);
		PlayerPrefs.SetInt (USER_LASTSCORE_KEY, _lastScore);


		ScoreModel.score = _userCoin;
		ScoreModel.bestScore = _bestScore;
	}

	void _checkValidData() {
		if (scoreType == ScoreType.Distance)
			Debug.Assert (pointAwardDistance > 0, "Invalid < 0");
		if (scoreType == ScoreType.Time)
			Debug.Assert (pointAwardTime > 0, "Invalid < 0");
	}

	void _initProcessor() {
		switch (scoreType) {
			case ScoreType.Distance:
				_processor = new ScoreDistanceProcessor (pointAwardDistance, (int)pointAwardPerDistance);
				break;
			case ScoreType.Time:
				_processor = new ScoreTimeProcessor (pointAwardTime, (int)pointAwardPerTime);
				break;
			case ScoreType.Coin:
				_processor = new ScoreCoinProcessor ();
				break;
			default:
				_processor = null;
				break;
		}
	}
	#endregion

	#region DELEGATE
	public void addCoinDelegate(ScoreShoingScriptDelegate dele) {
		Debug.Assert (dele.getAction() == ScoreAction.Coin, "Invalid action type");
		_coinDelegates.Add (dele);
	}
	public void removeCoinDelegate(ScoreShoingScriptDelegate dele) {
		_coinDelegates.Remove (dele);
	}
	public void addScoreDelegate(ScoreShoingScriptDelegate dele) {
		Debug.Assert (dele.getAction() == ScoreAction.Score, "Invalid action type");
		_scoreDelegates.Add (dele);
	}
	public void removeScoreDelegate(ScoreShoingScriptDelegate dele) {
		_scoreDelegates.Remove (dele);
	}
	#endregion
	#region DELEGATE DISPATCH
	private void _dispatchCoin() {
		foreach (var item in _coinDelegates) {
			item.update (_userCoin);
		}
	}

	private void _dispatchScore() {
		foreach (var item in _scoreDelegates) {
			item.update (_userScore);
		}
	}
	#endregion
}
