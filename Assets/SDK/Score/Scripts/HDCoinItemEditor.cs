﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;
using AssemblyCSharp;

[CustomEditor(typeof(HDCoinItemScript))]
[CanEditMultipleObjects]
public class HDCoinItemEditor : Editor {
	SerializedProperty actionPro;
	SerializedProperty valuePro;
	SerializedProperty animationDiePro;

	// Use this for initialization
	MonoScript script;

	void OnEnable() {
		script = MonoScript.FromMonoBehaviour((HDCoinItemScript)target);

		actionPro = serializedObject.FindProperty ("action");
		valuePro = serializedObject.FindProperty ("value");
		animationDiePro = serializedObject.FindProperty ("animationStateDie");
	}

	public override void OnInspectorGUI() {
		serializedObject.Update ();

		EditorGUI.BeginDisabledGroup (true);
		script = EditorGUILayout.ObjectField("Script:", script, typeof(MonoScript), false) as MonoScript;
		EditorGUI.EndDisabledGroup ();


		if (!_checkInitialize ()) {
			EditorGUILayout.HelpBox ("Required component:\n- Collider2D inheritance component", MessageType.Error);
			throw new UnityException ("Required component");
//			#error Oops. This is an error.

//			if (GUILayout.Button ("Initialize")) {
//				//(target as ScoreItemScript).gameObject.AddComponent<BoxCollider2D> ();
//
//			}
		} else {
			HDCoinItemScript.ItemActionType action = (HDCoinItemScript.ItemActionType)actionPro.enumValueIndex;
			EditorGUILayout.PropertyField (actionPro);
			if (action == HDCoinItemScript.ItemActionType.Score || action == HDCoinItemScript.ItemActionType.Coin) {
				EditorGUILayout.PropertyField (valuePro);
				EditorGUILayout.PropertyField (animationDiePro);
			} else {
				// custom action
				EditorGUILayout.HelpBox ("For custom action. Add your custom script plz", MessageType.Info);
			}
		}

		serializedObject.ApplyModifiedProperties ();
	}


	#region PRIVATE FUNC

	bool _checkInitialize() {
		if ((target as HDCoinItemScript).gameObject.GetComponent<Collider2D> () == null)
			return false;
		return true;
	}

	#endregion
}

#endif
