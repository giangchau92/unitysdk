﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;

public class HDCoinItemScript : MonoBehaviour {
	public enum ItemActionType
	{
		Score,
		Coin,
		Custome
	}
	public ItemActionType action;
	public uint value;
	public int animationStateDie;
	// Use this for initialization
	Animator _animator;
	void Start () {
		_animator = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.tag.Equals ("Player")) {
			if (action == ItemActionType.Score) {
				GameGlobal.getInstance ().ScoreManager.changeScore ((int)value);
			} else if (action == ItemActionType.Coin) {
				GameGlobal.getInstance ().ScoreManager.changeCoin ((int)value);
			}
			_die ();
		}
	}

	#region PRIVATE FUNC

	void _die() {
		_animator.SetInteger ("state", animationStateDie);
	}

	#endregion
}
