﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using AssemblyCSharp;

public interface ScoreShoingScriptDelegate
{
	HDScoreManagerScript.ScoreAction getAction();
	void update(int point);
}

public class ScoreShowingScript : MonoBehaviour, ScoreShoingScriptDelegate {
	public HDScoreManagerScript.ScoreAction action;

	// Use this for initialization
	Text _text;
	void Start () {
		// đăng kí delegate cho Score Manager
		_text = GetComponent<Text>();
		if (action == HDScoreManagerScript.ScoreAction.Score) {
			GameGlobal.getInstance ().ScoreManager.addScoreDelegate (this);
		} else if (action == HDScoreManagerScript.ScoreAction.Coin) {
			GameGlobal.getInstance ().ScoreManager.addCoinDelegate (this);
		}

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void update(int point) {
		_text.text = point.ToString ();
	}

	public HDScoreManagerScript.ScoreAction getAction() {
		return this.action;
	}

}
