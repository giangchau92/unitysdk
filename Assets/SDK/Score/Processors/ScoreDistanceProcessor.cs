﻿using System;
using UnityEngine;

namespace AssemblyCSharp
{
	public class ScoreDistanceProcessor : ScoreBaseProcessor
	{
		private Transform _character;
		private Vector3 _lastPosition;
		private Vector3 _direction;
		private double _totalDistance;
		private double _currentDistance;

		private double _pointAwardDistance;
		private int _pointAwardPreDistance;

		protected bool _didInit = false;

		public ScoreDistanceProcessor (double pointAwardDistance, int pointAwardPreDistance)
		{
			_pointAwardDistance = pointAwardDistance;
			_pointAwardPreDistance = pointAwardPreDistance;
		}

		private void _lazyInit() {
			if (!_didInit) {
				_character = GameGlobal.getInstance ().GameManager.character.transform;
				_lastPosition = _character.position;
				GameObject firstLevel = GameGlobal.getInstance ().GameManager.getLastShowingLevel ();
				_direction = LevelUtils.getEndPositionInWorld (firstLevel) - LevelUtils.getStartPositionInWorld (firstLevel);
				_didInit = true;
			}
		}

		public override void Update (double deltaTime)
		{
			_lazyInit ();
			Vector3 distance = _character.position - _lastPosition;
			_lastPosition = _character.position;
			Vector2 distanceInDirect = new Vector3 (distance.x, distance.x / _direction.x * _direction.y);
			_currentDistance += distanceInDirect.magnitude;
			if (_currentDistance > _pointAwardDistance) {
				GameGlobal.getInstance ().ScoreManager.changeScore (_pointAwardPreDistance);
				_currentDistance -= _pointAwardDistance;
			}
		}
	}
}

