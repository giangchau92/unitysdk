﻿using System;

namespace AssemblyCSharp
{
	public class ScoreTimeProcessor : ScoreBaseProcessor
	{
		private double _pointAwardTime;
		private int _pointAwardPerTime;

		private double _totalTime;
		private double _currentTime;

		public ScoreTimeProcessor (double pointAwardTime, int pointAwardPreTime)
		{
			_pointAwardTime = pointAwardTime;
			_pointAwardPerTime = pointAwardPreTime;
		}

		public override void Update (double deltaTime) {
			_currentTime += deltaTime;
			if (_currentTime > _pointAwardTime) {
				_currentTime -= _pointAwardTime;
				GameGlobal.getInstance ().ScoreManager.changeScore (_pointAwardPerTime);
			}
		}
	}
}

