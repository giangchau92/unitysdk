﻿using UnityEngine;
using System.Collections;

public class InhouseSDK {

	static InhouseSDK _instance;
	public static InhouseSDK getInstance() {
		if (_instance == null)
			_instance = new InhouseSDK ();
		return _instance;
	}

}
