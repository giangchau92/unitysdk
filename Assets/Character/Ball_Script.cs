﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;
using AssemblyCSharp;

public class Ball_Script : MonoBehaviour
{

	public float jumpForce = 1;
	public float speed = 1;
	public Sprite red;
	public Sprite yellow;
	public Sprite purple;
	public Sprite green;
	private Rigidbody2D myRigidbody;
	private ColorBlockItem_Script.BlockType currentType;

	public AudioClip[] listAudio;
	private AudioSource audioSource;
	// Use this for initialization
	void Start ()
	{
		myRigidbody = GetComponent<Rigidbody2D> ();
		this.audioSource = this.GetComponent<AudioSource> ();
		currentType = ColorBlockItem_Script.BlockType.Red;
	}
	
	// Update is called once per frame
	void Update ()
	{
		myRigidbody.velocity = new Vector2 (speed, myRigidbody.velocity.y);
		if (Input.GetMouseButtonDown (0)) {
			myRigidbody.velocity = new Vector2 (myRigidbody.velocity.x, jumpForce);
		}
		switch (currentType) {
		case ColorBlockItem_Script.BlockType.Red:
			gameObject.GetComponent<SpriteRenderer> ().sprite = red;
			break;
		case ColorBlockItem_Script.BlockType.Yellow:
			gameObject.GetComponent<SpriteRenderer> ().sprite = yellow;
			break;
		case ColorBlockItem_Script.BlockType.Purple:
			gameObject.GetComponent<SpriteRenderer> ().sprite = purple;
			break;
		case ColorBlockItem_Script.BlockType.Green:
			gameObject.GetComponent<SpriteRenderer> ().sprite = green;
			break;
		}
	}

	void OnTriggerEnter2D (Collider2D blockColor)
	{
		if (gameObject == null)
			return;
		if (blockColor.gameObject.tag == "Enemy") {
			SceneManager.LoadScene ("GameOver");
		}
		if (blockColor.gameObject.GetComponent<ColorBlockItem_Script> ().isEnable) {
			if (blockColor.GetComponent<ColorBlockItem_Script> ().type == currentType) {
				audioSource.PlayOneShot (listAudio [0], 1f);
				GameGlobal.getInstance ().ScoreManager.changeScore (1);
				changeColorBall ();
			} else {
				SceneManager.LoadScene ("GameOver");
			}
			blockColor.gameObject.GetComponent<ColorBlockItem_Script> ().isEnable = false;
		}
	}

	private void changeColorBall ()
	{
		Array values = Enum.GetValues (typeof(ColorBlockItem_Script.BlockType));
		System.Random rd = new System.Random ();
		ColorBlockItem_Script.BlockType randomBlock = (ColorBlockItem_Script.BlockType)values.GetValue (rd.Next (values.Length));
		currentType = randomBlock;
	}
}
