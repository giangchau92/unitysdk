﻿using UnityEngine;
using System.Collections;

public class GameOverManager : MonoBehaviour {

	public AudioClip[] listAudio;
	private AudioSource audioSource;

	// Use this for initialization
	void Start () {
		this.audioSource = this.GetComponent<AudioSource> ();
		audioSource.clip = listAudio [0];
		audioSource.Play ();
	}
	
	// Update is called once per frame
	void Update () {
	}
}
