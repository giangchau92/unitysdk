﻿using UnityEngine;
using System.Collections;

public class ZiZaScript : MonoBehaviour {
	public float speed;
	public Vector2 direct;
	public float rotateSpeed;
	public GameObject shadow;
	public float time;

	// Use this for initialization
	Rigidbody2D _rigid2D;
	Vector2 _currentDirect;
	float currentTime = 0;
	void Start () {
		_rigid2D = GetComponent<Rigidbody2D> ();
		_currentDirect = direct;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (0)) {
			swithDirection ();
		}

		currentTime += Time.deltaTime;
		if (currentTime > 0.05f) {
			Instantiate (shadow, transform.position, transform.rotation);
			currentTime = 0;
		}
	}

	void FixedUpdate() {
		_currentDirect = Vector2.Lerp (_currentDirect, direct, Time.deltaTime * rotateSpeed);
		Debug.Log (_currentDirect.ToString());
		Vector2 vel = calcVel (_currentDirect, speed);
		_rigid2D.velocity = new Vector2 (speed, vel.y);
	}

	Vector2 calcVel(Vector2 direction, float speed) {
		float length = direction.magnitude;
		float ratio = speed / length;
		return new Vector2 (direction.x * ratio, direction.y * ratio);
	}

	void swithDirection() {
		direct.y = -direct.y;
	}
}
