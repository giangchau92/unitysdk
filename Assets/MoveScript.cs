﻿using UnityEngine;
using System.Collections;

public class MoveScript : MonoBehaviour {
	public BezierSpline bezier;
	public float duration;

	float currentTime = 0;

	// Use this for initialization
	void Start () {
		transform.position = bezier.GetPoint (0);
	}
	
	// Update is called once per frame
	void Update () {
		if (currentTime < duration) {
			transform.position = bezier.GetPoint (currentTime / duration);
			currentTime += Time.deltaTime;
		}
	}
}
